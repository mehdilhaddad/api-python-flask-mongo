from flask import Flask, make_response, request, jsonify
from flask_mongoengine import MongoEngine
from api_constants import mongodb_password

# On lance d'abord l'application. 
app = Flask(__name__)
database_name = "monApi"
DB_URI = "mongodb+srv://mehdilhaddad:{}@pythoncluster.rupkr.mongodb.net/{}?retryWrites=true&w=majority".format(mongodb_password, database_name)
app.config["MONGODB_HOST"] = DB_URI
db = MongoEngine()
db.init_app(app)

# On créer ensuite un document.
class Veille(db.Document):
  veille_id = db.IntField()
  title = db.StringField()
  description = db.StringField()
  links = db.StringField()
  author = db.StringField()
  tag = db.StringField()

# Ici on convertit le document en JSON.
  def to_json(self):
    return {
      "veille_id": self.veille_id,
      "title": self.title,
      "description": self.description,
      "links": self.links,
      "author": self.author,
      "tag": self.tag,
    }

# On publie le document manuellement dans la base de donnée.
@app.route('/api/db_populate', methods=['POST'])
def db_populate():
  veille1 = Veille(veille_id=1, title="Ma veille", description="Bonjour Simplon", links="https://www.youtube.com/", author="Mehdi", tag="#veille")
  veille2 = Veille(veille_id=2, title="La veille du futur", description="Une Musique avec beaucoup de flow", links="https://www.youtube.com/watch?v=HRdSrgcoymc", author="Mangekyou", tag="#veille2")
  veille1.save()
  veille2.save()
  return make_response("", 201)

# On récupére et affiche tout les documents.
@app.route('/api/v1/topics', methods=['GET', 'POST'])
def api_veilles():
  if request.method == "GET":
    veilles = []
    for veille in Veille.objects:
      veilles.append(veille)
    return make_response(jsonify(veilles), 200)
  elif request.method == "POST":
    content = request.json
    veille = Veille(veille_id=content['veille_id'],
    title=content['title'],
    description=content['description'],
    links=content['links'],
    author=content['author'],
    tag=content['tag'])
    veille.save()
    return make_response("", 201)

# Ici récupére un document pour le modifier ou le supprime.
@app.route('/api/v1/topics/<veille_id>', methods=['GET', 'PUT', 'DELETE'])
def api_each_veille(veille_id):
  if request.method == "GET":
    veille_obj = Veille.objects(veille_id=veille_id).first()
    if veille_id:
      return make_response(jsonify(veille_obj.to_json()), 200)
    else:
      return make_response("", 404)
  elif request.method == "PUT":
    content = request.json
    veille_obj = Veille.objects(veille_id=veille_id).first()
    veille_obj.update(veille_id=content['veille_id'],
    title=content['title'],
    description=content['description'],
    links=content['links'],
    author=content['author'],
    tag=content['tag'])
    return make_response("", 204)
  elif request.method == "DELETE":
    veille_obj = Veille.objects(veille_id=veille_id).first()
    veille_obj.delete()
    return make_response("", 204)

# On execute l'application.
if __name__ == '__main__':
    app.run()