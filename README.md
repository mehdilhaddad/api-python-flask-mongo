# Projet : api-python-flask-mongo

## Installation du projet

1. Installation du fichier d'environnement:

`python3 -m venv env`

2. Activation du fichier d'environnement:

`source env/bin/activate`

## Lancement du serveur

`python3 app.py`

## Pour tester les requêtes

`pip3 install httpie`
